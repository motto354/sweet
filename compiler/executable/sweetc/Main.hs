module Main
	( main
	) where

import Control.Applicative ((<**>), many)
import Control.Monad.Writer (execWriter)
import Data.Foldable (fold)
import Data.Set (Set)
import Data.Text (Text)
import Data.Text.Lazy.Encoding (encodeUtf8)
import System.Exit (exitFailure)
import System.IO (hPrint, stderr)

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Set as Set
import qualified Data.Text.Lazy.Builder as TB
import qualified Options.Applicative as O

import Sweet.Analyze.Infer (inferTranslationUnit)
import Sweet.Syntax.Source (TranslationUnit (..))
import Sweet.Textual.SourceLex (lexSource)
import Sweet.Textual.SourceParse (parse)
import Sweet.Transform.ANFify (anfifyTranslationUnit)
import Sweet.Transform.Resolve (resolveTranslationUnit, runResolve)
import Sweet.Translate.ECMAScript (translateTranslationUnit)

data Options = Options
	{ optionsObject :: String
	, optionsInterface :: String
	, optionsDefines :: Set Text
	, optionsSources :: [String] }
	deriving stock (Show)

optionParser :: O.ParserInfo Options
optionParser = O.info (parser <**> O.helper) info
	where
	parser = do
		object <- O.strOption $ fold
			[ O.long "object"
			, O.metavar "OBJECT"
			, O.help "Object file to generate" ]
		interface <- O.strOption $ fold
			[ O.long "interface"
			, O.metavar "INTERFACE"
			, O.help "Interface file to generate" ]
		defines <- fmap Set.fromList . many . O.strOption $ fold
			[ O.long "define"
			, O.metavar "DEFINE ..."
			, O.help "Enable particular #ifdef ... #endif" ]
		sources <- many . O.argument O.str $ fold
			[ O.metavar "SOURCES ..."
			, O.help "Source files to compile" ]
		pure $ Options object interface defines sources

	info = fold
		[ O.fullDesc
		, O.progDesc "Compiler for the Sweet programming language" ]

main :: IO ()
main = do
	options <- O.execParser optionParser

	sources <- traverse LBS.readFile (optionsSources options)
	let tokens = lexSource (optionsDefines options) <$> sources
	let sourceTU = TranslationUnit $ foldMap parse tokens

	let resolvedTU = runResolve resolveTranslationUnit sourceTU
	case inferTranslationUnit resolvedTU of
		errors@(_ : _) -> do
			hPrint stderr errors
			exitFailure
		[] -> do
			let anfTU = anfifyTranslationUnit resolvedTU
			let ecmascript = encodeUtf8 . TB.toLazyText . execWriter $
				translateTranslationUnit anfTU
			LBS.writeFile (optionsObject options) ecmascript
