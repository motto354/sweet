let
	nixpkgs = import ../nix/nixpkgs.nix;
	ghc = import ./nix/ghc.nix;
	ghcWithPackages =
		nixpkgs.haskell.packages.${ghc}.ghcWithPackages (h:
			[h.cabal-install h.ghcid] ++
			import ./nix/dependencies.nix h
		);
in
	nixpkgs.stdenv.mkDerivation {
		name = "sweetc-nix-shell";
		buildInputs = [ghcWithPackages];
	}
