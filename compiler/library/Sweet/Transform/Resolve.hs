-- | Name resolution finds unqualified names and turns them into qualified
-- names. The algorithm is as follows:
--
--  * Nothing happens to names that are already qualified.
--  * Nothing happens to names of intrinsics.
--  * Nothing happens to names of local variables.
--  * In the scope of a using directive mentioning a certain name, that name is
--    qualified with the using directive as appropriate.
--  * Otherwise, the name is qualified with the current namespace.
--
-- This phase also completely removes all using directives. Due to the use of
-- GADTs with 'Qualifiedness', the Haskell type checker guarantees this as
-- well.
module Sweet.Transform.Resolve
	( Resolver
	, Resolve
	, Usings
	, runResolve
	, resolveGlobal
	, resolveTranslationUnit
	, resolveDefinition
	, resolveExpression
	, resolveType
	) where

import Control.Lens ((%~), _1, _2, at, view)
import Control.Monad (join)
import Control.Monad.Reader (Reader, runReader)
import Data.Map.Strict (Map)
import Data.Maybe (fromMaybe)
import Data.Semigroup ((<>))
import Data.Traversable (for)

import qualified Control.Monad.Reader as Reader
import qualified Data.Map.Strict as Map

import Sweet.Name (Global (..), Identifier, Namespace, Qualifiedness (..), rootNamespace, snocNamespace)
import Sweet.Syntax.Source (Definition (..), Expression (..), Kind (..), TranslationUnit (..), Type (..))

-- | A Kleisli arrow in the 'Resolve' monad that resolves names of some syntax
-- node described by @f@. Returns the same node, but with names resolved.
type Resolver f =
	f 'U -> Resolve (f 'Q)

-- | A monad for resolving names.
type Resolve =
	Reader (Namespace, Usings)

-- | Mapping from names to namespaces, the opposite of a using definition.
-- Allows efficient lookup from unqualified name to the namespace it should be
-- qualified with.
type Usings =
	Map Identifier Namespace

-- | Run a resolve action in the root namespace with no using directives in
-- the lexical scope.
runResolve :: Resolver f -> f 'U -> f 'Q
runResolve r f = runReader (r f) (rootNamespace, Map.empty)

-- | Resolve a name of a global, qualifying it if necessary.
resolveGlobal :: Resolver Global
resolveGlobal (Qualified ns n) = pure $ Qualified ns n
resolveGlobal (Intrinsic n) = pure $ Intrinsic n
resolveGlobal (Unqualified n) = do
	ns <- fromMaybe <$> view _1 <*> view (_2 . at n)
	pure $ Qualified ns n

-- | Resolve names in a translation unit.
resolveTranslationUnit :: Resolver (TranslationUnit Expression)
resolveTranslationUnit (TranslationUnit ds) =
	TranslationUnit . join <$> traverse resolveDefinition ds

-- | Resolve names in a definition. Flattens 'UsingDefinition' objects, hence
-- the list return type.
resolveDefinition
	:: Definition Expression 'U
	-> Resolve [Definition Expression 'Q]

resolveDefinition (NamespaceDefinition ns ds) = do
	ds' <- for ds $
		Reader.local (_1 %~ (`snocNamespace` ns)) .
			resolveDefinition
	pure [NamespaceDefinition ns (join ds')]

resolveDefinition (UsingDefinition ns us ds) = do
	ds' <- withUsings ns us $ traverse resolveDefinition ds
	pure (join ds')

resolveDefinition (DomainDefinition n t) = do
	t' <- resolveType t
	pure [DomainDefinition n t']

resolveDefinition (FunctionDefinition n ts ps r e) = do
	ts' <- traverse (traverse resolveKind) ts
	ps' <- traverse (traverse resolveType) ps
	r' <- resolveType r
	e' <- resolveExpression e
	pure [FunctionDefinition n ts' ps' r' e']

-- | Resolve names in an expression.
resolveExpression :: Resolver Expression

resolveExpression (UsingExpression ns us e) = do
	withUsings ns us $ resolveExpression e

resolveExpression (LocalExpression n) =
	pure $ LocalExpression n

resolveExpression (USVLiteralExpression t) =
	pure $ USVLiteralExpression t

resolveExpression (TextLiteralExpression t) =
	pure $ TextLiteralExpression t

resolveExpression (CallExpression f es) = do
	f' <- resolveGlobal f
	es' <- traverse resolveExpression es
	pure $ CallExpression f' es'

resolveExpression (ApplyExpression f xs) = do
	f' <- resolveExpression f
	xs' <- traverse resolveExpression xs
	pure $ ApplyExpression f' xs'

resolveExpression (LambdaExpression ps b) = do
	b' <- resolveExpression b
	pure $ LambdaExpression ps b'

resolveExpression (WrapExpression d e) = do
	d' <- resolveGlobal d
	e' <- resolveExpression e
	pure $ WrapExpression d' e'

resolveExpression (UnwrapExpression d e) = do
	d' <- resolveGlobal d
	e' <- resolveExpression e
	pure $ UnwrapExpression d' e'

-- | Resolve names in a type.
resolveType :: Resolver Type

resolveType (UsingType ns us t) =
	withUsings ns us $ resolveType t

resolveType (LocalType n) =
	pure $ LocalType n

resolveType (FunctionType ps r) = do
	ps' <- traverse resolveType ps
	r' <- resolveType r
	pure $ FunctionType ps' r'

resolveType (ForAllType n k t) = do
	t' <- resolveType t
	k' <- resolveKind k
	pure $ ForAllType n k' t'

resolveType (CallType f es) = do
	f' <- resolveGlobal f
	es' <- traverse resolveType es
	pure $ CallType f' es'

resolveType EmptyRowType =
	pure EmptyRowType

-- | Resolve names in a kind.
resolveKind :: Resolver Kind

resolveKind TypeKind =
	pure TypeKind

resolveKind (RowKind k) = do
	k' <- resolveKind k
	pure $ RowKind k'

resolveKind (FunctionKind ps r) = do
	ps' <- traverse resolveKind ps
	r' <- resolveKind r
	pure $ FunctionKind ps' r'

-- | Given a namespace and a list of names, run an action in an environment
-- that is the body of any @using@ node.
withUsings :: Namespace -> [Identifier] -> Resolve a -> Resolve a
withUsings ns us =
	let usings = foldr (`Map.insert` ns) Map.empty us
	in Reader.local (_2 %~ (usings <>))
