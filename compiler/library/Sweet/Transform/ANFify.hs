-- | Turn source syntax into ANF. ANF is a program representation more suitable
-- for code generation than the source syntax is. To learn more about ANF, and
-- our implementation of it in particular, see the "Sweet.Syntax.ANF" module.
module Sweet.Transform.ANFify
	( -- * Infrastructure
	  ANFify
	, runANFify
	, freshLocal

	  -- * ANFification of source
	, anfifyTranslationUnit
	, anfifyDefinition
	, anfifyExpression
	) where

import Control.Lens ((<<+=))
import Control.Monad.Cont (ContT (..), runContT)
import Control.Monad.State (State, evalState)
import Control.Monad.Trans (lift)

import Sweet.Name (Local (SynthesizedLocal), Qualifiedness (Q))

import qualified Sweet.Syntax.ANF as A
import qualified Sweet.Syntax.Source as S

-- | Monad needed for generating fresh names for let bindings in ANF. These let
-- bindings are introduced for nested expressions, such as function arguments.
type ANFify =
	State Word

-- | Run an 'ANFify' computation.
runANFify :: ANFify a -> a
runANFify = evalState `flip` 0

-- | Generate a new local variable in an 'ANFify' computation. This new
-- variable will not clash with any user-defined variables nor other variables
-- generated in the same computation.
freshLocal :: ANFify Local
freshLocal = SynthesizedLocal <$> (id <<+= 1)

-- | Turn all expressions in translation unit into ANF.
anfifyTranslationUnit
	:: S.TranslationUnit S.Expression 'Q
	-> A.TranslationUnit A.Expression 'Q
anfifyTranslationUnit (S.TranslationUnit ds) =
	A.TranslationUnit (anfifyDefinition <$> ds)

-- | Turn all expressions in a definition into ANF.
anfifyDefinition
	:: S.Definition S.Expression 'Q
	-> A.Definition A.Expression 'Q

anfifyDefinition (S.NamespaceDefinition n ds) =
	A.NamespaceDefinition n (anfifyDefinition <$> ds)

anfifyDefinition (S.DomainDefinition n t) =
	A.DomainDefinition n t

anfifyDefinition (S.FunctionDefinition n ts ps r b) =
	let b' = runANFify (runContT (anfifyExpression b) (pure . A.ValueExpression)) in
	A.FunctionDefinition n ts ps r b'

-- | Turn an expression into ANF. This function uses continuation-passing style
-- because it sometimes wraps the continuations in ANF let bindings. The
-- 'ContT' monad transformer is used because it makes it straightforward to
-- compose continuation-passing style computations, specifically those that
-- contain loops (using 'traverse').
anfifyExpression
	:: S.Expression 'Q
	-> ContT (A.Expression 'Q) ANFify (A.Value 'Q)

anfifyExpression (S.LocalExpression a) =
	pure $ A.LocalValue a

anfifyExpression (S.USVLiteralExpression a) =
	pure $ A.USVLiteralValue a

anfifyExpression (S.TextLiteralExpression a) =
	pure $ A.TextLiteralValue a

anfifyExpression (S.CallExpression f as) = do
	as' <- traverse anfifyExpression as
	result <- lift freshLocal
	ContT $ \cont -> do
		cont' <- cont (A.LocalValue result)
		pure $ A.LetExpression result (A.CallExpression f as') cont'

anfifyExpression (S.ApplyExpression f as) = do
	f' <- anfifyExpression f
	as' <- traverse anfifyExpression as
	result <- lift freshLocal
	ContT $ \cont -> do
		cont' <- cont (A.LocalValue result)
		pure $ A.LetExpression result (A.ApplyExpression f' as') cont'

anfifyExpression (S.LambdaExpression ps b) = do
	b' <- lift $ runContT (anfifyExpression b) (pure . A.ValueExpression)
	pure $ A.LambdaValue ps b'

anfifyExpression (S.WrapExpression t e) = do
	e' <- anfifyExpression e
	pure $ A.WrapValue t e'

anfifyExpression (S.UnwrapExpression t e) = do
	e' <- anfifyExpression e
	pure $ A.UnwrapValue t e'
