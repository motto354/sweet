-- | Administrative normal form source representation. This representation is
-- useful for code generation, because target languages typically do not
-- support arbitrary nesting of expressions, or do not have a well-defined
-- order of evaluation. To learn more, see the
-- <https://en.wikipedia.org/wiki/A-normal_form A-normal form> article on
-- Wikipedia.
--
-- The 'Expression' and 'Value' constructors that correspond to similarly-named
-- ones in "Sweet.Syntax.Source" are not documented; their semantics are the
-- same as those in the source syntax.
module Sweet.Syntax.ANF
	( TranslationUnit (..)
	, Definition (..)
	, Expression (..)
	, Value (..)
	) where

import Data.Text (Text)

import Sweet.Name (Global, Local, Qualifiedness)
import Sweet.Syntax.Source (Definition (..), TranslationUnit (..))

-- | An expression may contain other expressions and values.
data Expression :: Qualifiedness -> * where
	-- | Evaluate to a value, in constant time.
	ValueExpression :: Value q -> Expression q

	LetExpression :: Local -> Expression q -> Expression q -> Expression q

	CallExpression :: Global q -> [Value q] -> Expression q

	ApplyExpression :: Value q -> [Value q] -> Expression q

deriving stock instance Eq (Expression q)
deriving stock instance Show (Expression q)

-- | A value does not contain other expressions, except as part of a lambda
-- body.
data Value :: Qualifiedness -> * where
	LocalValue :: Local -> Value q
	USVLiteralValue :: Char -> Value q
	TextLiteralValue :: Text -> Value q
	LambdaValue :: [Local] -> Expression q -> Value q
	WrapValue :: Global q -> Value q -> Value q
	UnwrapValue :: Global q -> Value q -> Value q

deriving stock instance Eq (Value q)
deriving stock instance Show (Value q)
