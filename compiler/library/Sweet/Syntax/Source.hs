-- | Abstract syntax trees for source files. This represents a program but
-- without all the syntax necessary for unambiguous parsing. It is hence a
-- simplified representation.
--
-- The 'Eq' instances of the data types defined in this module report
-- /syntactical/ equality. This is to say that if, for instance, two function
-- definitions define functions that produce the same output for the same
-- inputs, but the implementations are not syntactically the same, they are
-- considered distinct nonetheless.
module Sweet.Syntax.Source
	( TranslationUnit (..)
	, Definition (..)
	, Expression (..)
	, Type (..)
	, Kind (..)
	) where

import Data.Text (Text)

import Sweet.Name (Global, Identifier, Local, Namespace, Qualifiedness (..))

-- | A translation unit is just a list of top-level definitions. These are
-- read from zero or more source files and their order is not significant.
-- Parameterized over expression type, since this is reused for both source and
-- ANF forms.
newtype TranslationUnit f q =
	TranslationUnit [Definition f q]
	deriving stock (Eq, Show)

-- | A top-level definition
--
--  - defines a new type;
--  - defines a new function;
--  - sets the current namespace; or
--  - brings definitions from other namespaces into scope.
--
-- Expressions and types may refer to definitions using 'Global'. 'Global' is
-- used for both expressions and types, which is safe since those live in
-- separate namespaces.
--
-- Parameterized over expression type, since this is reused for both source and
-- ANF forms.
data Definition :: (Qualifiedness -> *) -> Qualifiedness -> * where
	-- | A namespace definition does not really define anything, but it
	-- syntactically occurs where definitions may occur. A namespace
	-- definition sets the namespace for its contents. Namespace
	-- definitions may be nested, in which case subnamespaces will be
	-- defined. In the below example, the @abs@ function is defined in
	-- the @std::math@ namespace.
	--
	-- @
	-- namespace std {
	-- 	namespace math {
	-- 		func abs(a float) float =
	-- 			a < 0 ? -a : a;
	-- 	}
	-- }
	-- @
	NamespaceDefinition :: Identifier -> [Definition f q] -> Definition f q

	-- | A using definition does not really define anything, but it
	-- syntactically occurs where definitions may occur. A using definition
	-- brings names from other namespaces into the lexical scope.
	UsingDefinition :: Namespace -> [Identifier] -> [Definition f 'U] -> Definition f 'U

	-- | A domain creates a new type that wraps an old type.
	DomainDefinition :: Identifier -> Type q -> Definition f q

	-- | A function definition defines a function. A function has a list of
	-- type parameters, a list of value parameters, a return type, and a
	-- body which is an expression.
	FunctionDefinition :: Identifier -> [(Local, Kind q)] -> [(Local, Type q)] -> Type q -> f q -> Definition f q

deriving stock instance (Eq (f q)) => Eq (Definition f q)
deriving stock instance (Show (f q)) => Show (Definition f q)

-- | An expression may be evaluated, turning it into a value. This is how
-- computation happens.
data Expression :: Qualifiedness -> * where
	-- | A using expression is similar to a using definition, but occurs in
	-- expression context. Its nested expression has in scope definitions
	-- from another namespace.
	UsingExpression :: Namespace -> [Identifier] -> Expression 'U -> Expression 'U

	-- | A local variable. When evaluated, yields the value of the
	-- variable. This variable must always be local, since there are no
	-- global variables in the language.
	LocalExpression :: Local -> Expression q

	-- | An expression that evaluates to the same USV regardless of the
	-- context.
	USVLiteralExpression :: Char -> Expression q

	-- | An expression that evaluates to the same text regardless of the
	-- context.
	TextLiteralExpression :: Text -> Expression q

	-- | Call a defined function with zero or more arguments. The function
	-- is a global. To call a local function, use 'ApplyExpression'
	-- instead.
	CallExpression :: Global q -> [Expression q] -> Expression q

	-- | Call a closure with zero or more arguments. The function is an
	-- arbitrary expression, not a global. For global functions, see
	-- 'CallExpression'.
	ApplyExpression :: Expression q -> [Expression q] -> Expression q

	-- | Closure literal, of a function type.
	LambdaExpression :: [Local] -> Expression q -> Expression q

	-- | Wrap a value in a domain.
	WrapExpression :: Global q -> Expression q -> Expression q

	-- | Unwrap a value in a domain.
	UnwrapExpression :: Global q -> Expression q -> Expression q

deriving stock instance Eq (Expression q)
deriving stock instance Show (Expression q)

-- | I'm still not really sure how to define /type/. But hey, this ADT
-- represents the possible types.
data Type :: Qualifiedness -> * where
	-- | A type used to continue type inference even if there are type
	-- errors, so that multiple errors can be collected. Not available
	-- syntactically, but synthesized during type inference.
	ErrorType :: Type 'Q

	-- | A using type is similar to a using definition, but occurs in type
	-- context. Its nested type has in scope definitions from another
	-- namespace.
	UsingType :: Namespace -> [Identifier] -> Type 'U -> Type 'U

	-- | A type that was not yet unified with a known type. Not available
	-- syntactically, but synthesized during type inference. Types may
	-- remain unknown after type inference. This is fine since types are
	-- not used in later compilation phases such as code generation.
	UnknownType :: Word -> Type 'Q

	-- | A type that will not unify with other known types. Not available
	-- syntactically, but synthesized during type inference. Skolems are
	-- completely abstract, because types are not reified at runtime.
	SkolemType :: Word -> Type 'Q

	-- | A local type variable, such as bound by 'ForAllType'. For global
	-- types, see 'NominalType'.
	LocalType :: Local -> Type q

	-- | A function type, for functions. Contains parameter types and
	-- return type.
	FunctionType :: [Type q] -> Type q -> Type q

	-- | A universally quantified type. A value of such a type is
	-- polymorphic. For example, a value of type @\<$a *> func(a) a@ has
	-- type @func(int) int@, type @func(double) double@, and many other
	-- types.
	ForAllType :: Local -> Kind q -> Type q -> Type q

	-- | Call a defined type with zero or more arguments. The type is a
	-- global. To call a local type, use 'ApplyType' instead.
	CallType :: Global q -> [Type q] -> Type q

	-- | The empty row type.
	EmptyRowType :: Type q

deriving stock instance Eq (Type q)
deriving stock instance Show (Type q)

-- | Kinds of types.
data Kind :: Qualifiedness -> * where
	-- | Like 'ErrorType'.
	ErrorKind :: Kind 'Q

	-- | The kind of types that contain values.
	TypeKind :: Kind q

	-- | The kind of row types.
	RowKind :: Kind q -> Kind q

	-- | For type constructors.
	FunctionKind :: [Kind q] -> Kind q -> Kind q

deriving stock instance Eq (Kind q)
deriving stock instance Show (Kind q)
