{
{-# LANGUAGE NoStrictData #-}

module Sweet.Textual.SourceParse where

import qualified Data.Map.Strict as Map

import qualified Sweet.Name as N
import qualified Sweet.Name.Intrinsic as I
import qualified Sweet.Syntax.Source as S
import qualified Sweet.Textual.SourceLex as L
}

%name parse
%error { (error . show) }
%tokentype { L.Token }

%token
	kdo { L.KDo }
	kdomain { L.KDomain }
	kfunc { L.KFunc }
	klet { L.KLet }
	knamespace { L.KNamespace }
	koperator { L.KOperator }
	kunwrap { L.KUnwrap }
	kusing { L.KUsing }
	kwrap { L.KWrap }

	pcoloncolon { L.PColonColon }
	plessgreater { L.PLessGreater }

	pasterisk { L.PAsterisk }
	pbang { L.PBang }
	pbracel { L.PBraceL }
	pbracer { L.PBraceR }
	pcomma { L.PComma }
	pdollar { L.PDollar }
	pequals { L.PEquals }
	pgreater { L.PGreater }
	phash { L.PHash }
	pless { L.PLess }
	pminus { L.PMinus }
	pparenl { L.PParenL }
	pparenr { L.PParenR }
	pperiod { L.PPeriod }
	pplus { L.PPlus }
	psemicolon { L.PSemicolon }
	pslash { L.PSlash }

	identifier { L.Identifier $$ }

	usvliteral { L.USVLiteral $$ }
	textliteral { L.TextLiteral $$ }

%%

SourceFile
	: Definitions { $1 }



Identifiers
	: { [] }
	| Identifier { [$1] }
	| Identifier pcomma Identifiers { $1 : $3 }

Identifier
	: identifier { N.Identifier $1 }
	| koperator pasterisk { N.OperatorAsterisk }
	| koperator plessgreater { N.OperatorLessGreater }
	| koperator pminus { N.OperatorMinus }
	| koperator pplus { N.OperatorPlus }
	| koperator pslash { N.OperatorSlash }

Namespace
	: Identifier { N.Namespace [$1] }
	| Identifier pcoloncolon Namespace
		{
			let N.Namespace ns = $3 in
			N.Namespace ($1 : ns)
		}

Global
	: Identifier { N.Unqualified $1 }
	| Namespace pcoloncolon Identifier { N.Qualified $1 $3 }
	| phash identifier { N.Intrinsic (I.intrinsics Map.! $2) }

Locals
	: { [] }
	| Local { [$1] }
	| Local pcomma Locals { $1 : $3 }

Local
	: pdollar Identifier { N.Local $2 }



Definitions
	: { [] }
	| Definition Definitions { $1 : $2 }

Definition
	: NamespaceDefinition { $1 }
	| UsingDefinition { $1 }
	| DomainDefinition { $1 }
	| FunctionDefinition { $1 }

NamespaceDefinition
	: knamespace Identifier pbracel Definitions pbracer
		{ S.NamespaceDefinition $2 $4 }

UsingDefinition
	: kusing Namespace pparenl Identifiers pparenr psemicolon Definitions
		{ S.UsingDefinition $2 $4 $7 }

DomainDefinition
	: kdomain Identifier pequals Type psemicolon
		{ S.DomainDefinition $2 $4 }

FunctionDefinition
	: kfunc Identifier TypeParametersDelimited ValueParametersDelimited Type pequals Expression psemicolon
		{ S.FunctionDefinition $2 $3 $4 $5 $7 }



Expressions
	: { [] }
	| Expression { [$1] }
	| Expression pcomma Expressions { $1 : $3 }

Expression
	: Expression1 { $1 }

Expression1
	: WrapExpression { $1 }
	| UnwrapExpression { $1 }
	| Expression2 { $1 }

WrapExpression
	: kwrap Global Expression1 { S.WrapExpression $2 $3 }

UnwrapExpression
	: kunwrap Global Expression1 { S.UnwrapExpression $2 $3 }

Expression2
	: InfixAsteriskExpression { $1 }
	| InfixSlashExpression { $1 }
	| Expression3 { $1 }

InfixAsteriskExpression
	: Expression2 pasterisk Expression3
		{ S.CallExpression (N.Unqualified N.OperatorAsterisk) [$1, $3] }

InfixSlashExpression
	: Expression2 pslash Expression3
		{ S.CallExpression (N.Unqualified N.OperatorSlash) [$1, $3] }

Expression3
	: InfixLessGreaterExpression { $1 }
	| Expression4 { $1 }

InfixLessGreaterExpression
	: Expression3 plessgreater Expression4
		{ S.CallExpression (N.Unqualified N.OperatorLessGreater) [$1, $3] }

Expression4
	: ApplyExpression { $1 }
	| Expression5 { $1 }

ApplyExpression
	: Expression4 pperiod pparenl Expressions pparenr { S.ApplyExpression $1 $4 }

Expression5
	: LocalExpression { $1 }
	| CallExpression { $1 }
	| LambdaExpression { $1 }
	| DoExpression { $1 }

LocalExpression
	: Local { S.LocalExpression $1 }

CallExpression
	: Global usvliteral { S.CallExpression $1 [S.USVLiteralExpression $2] }
	| Global textliteral { S.CallExpression $1 [S.TextLiteralExpression $2] }
	| Global pparenl Expressions pparenr { S.CallExpression $1 $3 }
	| Global { S.CallExpression $1 [] }

LambdaExpression
	: kfunc pparenl Locals pparenr pequals Expression
		{ S.LambdaExpression $3 $6 }

DoExpression
	: kdo pbracel Statement pbracer { $3 }



Statement
	: LetBangStatement { $1 }
	| ExpressionStatement { $1 }

LetBangStatement
	: klet pbang Local pequals Expression psemicolon Statement
		{
			let k = S.LambdaExpression [$3] $7 in
			S.CallExpression (N.Unqualified "bind") [$5, k]
		}

ExpressionStatement
	: Expression psemicolon { $1 }



Types
	: { [] }
	| Type { [$1] }
	| Type pcomma Types { $1 : $3 }

Type
	: Type1 { $1 }

Type1
	: CallType { $1 }
	| LocalType { $1 }
	| FunctionType { $1 }

CallType
	: Global { S.CallType $1 [] }
	| Global pparenl Types pparenr { S.CallType $1 $3 }

LocalType
	: Local { S.LocalType $1 }

FunctionType
	: kfunc pparenl Types pparenr Type { S.FunctionType $3 $5 }



Kind
	: Kind1 { $1 }

Kind1
	: TypeKind { $1 }

TypeKind
	: pasterisk { S.TypeKind }



TypeParametersDelimited
	: { [] }
	| pless TypeParameters pgreater { $2 }

TypeParameters
	: { [] }
	| TypeParameter { [$1] }
	| TypeParameter pcomma TypeParameters { $1 : $3 }

TypeParameter
	: Local Kind { ($1, $2) }

ValueParametersDelimited
	: { [] }
	| pparenl ValueParameters pparenr { $2 }

ValueParameters
	: { [] }
	| ValueParameter { [$1] }
	| ValueParameter pcomma ValueParameters { $1 : $3 }

ValueParameter
	: Local Type { ($1, $2) }
