{
{-# LANGUAGE NoStrictData #-}

module Sweet.Textual.SourceLex
	( lexSource
	, Token (..)
	) where

import Data.ByteString.Lazy (ByteString)
import Data.Set (Set)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Set as Set
import qualified Data.Text as Text
}

%wrapper "posn-bytestring"

token :-
	[\ \t\f\v\r\n] ;
	\/\/.* ;

	\#else { \_ _ -> HElse }
	\#endif { \_ _ -> HEndif }
	\#error { \_ _ -> HError }
	\#ifdef { \_ _ -> HIfdef }
	\#warning { \_ _ -> HWarning }

	do { \_ _ -> KDo }
	domain { \_ _ -> KDomain }
	func { \_ _ -> KFunc }
	let { \_ _ -> KLet }
	namespace { \_ _ -> KNamespace }
	operator { \_ _ -> KOperator }
	unwrap { \_ _ -> KUnwrap }
	using { \_ _ -> KUsing }
	wrap { \_ _ -> KWrap }

	\<\> { \_ _ -> PLessGreater }
	\:\: { \_ _ -> PColonColon }

	\- { \_ _ -> PMinus }
	\, { \_ _ -> PComma }
	\; { \_ _ -> PSemicolon }
	\! { \_ _ -> PBang }
	\. { \_ _ -> PPeriod }
	\( { \_ _ -> PParenL }
	\) { \_ _ -> PParenR }
	\{ { \_ _ -> PBraceL }
	\} { \_ _ -> PBraceR }
	\* { \_ _ -> PAsterisk }
	\/ { \_ _ -> PSlash }
	\# { \_ _ -> PHash }
	\+ { \_ _ -> PPlus }
	\< { \_ _ -> PLess }
	\= { \_ _ -> PEquals }
	\> { \_ _ -> PGreater }
	\$ { \_ _ -> PDollar }

	[a-zA-Z][a-zA-Z0-9]* { \_ -> Identifier . decodeUtf8 . LBS.toStrict }

	-- TODO: Handle escape sequences.
	\'[~\']*\' { \_ -> USVLiteral . Text.head . Text.tail . decodeUtf8 . LBS.toStrict }
	\"[~\"]*\" { \_ -> TextLiteral . Text.init . Text.tail . decodeUtf8 . LBS.toStrict }

{
-- | Lex source text, given a set of defines. The defines are those that may
-- appear in @#ifdef@ conditions.
lexSource :: Set Text -> ByteString -> [Token]
lexSource defines = go 0 . alexScanTokens
	where
	go :: Word -> [Token] -> [Token]

	go n (HIfdef : Identifier option : toks)
		| option `Set.member` defines = go (n + 1) toks
		| otherwise = go n (skipUntilEndif 0 toks)
	go _ (HIfdef : _) = error "lexSource: ill-formed #ifdef"

	go 0 (HEndif : _) = error "lexSource: stray #endif"
	go n (HEndif : toks) = go (n - 1) toks

	go n (tok : toks) = tok : go n toks
	go 0 [] = []
	go _ [] = error "lexSource: missing #endif"

	skipUntilEndif :: Word -> [Token] -> [Token]

	skipUntilEndif n (HIfdef : Identifier _ : toks) = skipUntilEndif (n + 1) toks
	skipUntilEndif _ (HIfdef : _) = error "lexSource: ill-formed #ifdef"

	skipUntilEndif 0 (HEndif : toks) = toks
	skipUntilEndif n (HEndif : toks) = skipUntilEndif (n - 1) toks

	skipUntilEndif n (_ : toks) = skipUntilEndif n toks
	skipUntilEndif _ [] = error "lexSource: missing #endif"

data Token :: * where

	HElse :: Token
	HEndif :: Token
	HError :: Token
	HIfdef :: Token
	HWarning :: Token

	KDo :: Token
	KDomain :: Token
	KFunc :: Token
	KLet :: Token
	KNamespace :: Token
	KOperator :: Token
	KUnwrap :: Token
	KUsing :: Token
	KWrap :: Token

	PColonColon :: Token
	PLessGreater :: Token

	PAsterisk :: Token
	PBang :: Token
	PBraceL :: Token
	PBraceR :: Token
	PComma :: Token
	PDollar :: Token
	PEquals :: Token
	PGreater :: Token
	PHash :: Token
	PLess :: Token
	PMinus :: Token
	PParenL :: Token
	PParenR :: Token
	PPeriod :: Token
	PPlus :: Token
	PSemicolon :: Token
	PSlash :: Token

	Identifier :: Text -> Token

	USVLiteral :: Char -> Token
	TextLiteral :: Text -> Token

deriving stock instance Eq Token
deriving stock instance Show Token
}
