-- | Name mangling turns names into identifiers suitable for use in target
-- languages. Target languages have various restrictions, which name mangling
-- aims to overcome. Because the same name mangling scheme is used for all
-- targets, the restrictions are very conservative, namely that the mangled
-- names all match the following regular expression: @^swt[a-z0-9]{0,60}$@.
-- This should be sufficient to overcome target language limitations such as
-- case-insensitivity, reserved words, lack of Unicode support, and reasonable
-- upper length limits.
--
-- There are two mangling phases: premangling and postmangling. Premangling is
-- straightforward:
--
--  * identifiers are prefixed with their lengths and an /i/;
--  * operators are prefixed with their lengths and an /o/;
--  * globals are prefixed with their arities and a /g/, followed by all
--    segments mangled and concatenated; and
--  * synthesized locals are prefixed with an /s/.
--
-- Postmangling takes the output of premangling, prefixes it with /swt/ and a
-- hash of the premangled text, truncates it to 63 characters, and makes it
-- lowercase.
--
-- The hash algorithm used is FNV-1a (32-bit) and the hash is encoded in
-- lowercase base 36.
module Sweet.Name.Mangle
	( -- * Mangling
	  Arity
	, mangleGlobal
	, mangleGlobal'
	, mangleLocal

	  -- * Premangling
	, premangleIdentifier
	, premangleGlobal
	, premangleGlobal'
	, premangleLocal

	  -- * Postmangling
	, postmangle
	) where

import Data.Char (ord)
import Data.Hash.FNV1a (fnv1a)
import Data.List (genericIndex)
import Data.Text (Text)
import Data.Word (Word32)

import qualified Data.Text as Text

import Sweet.Name (Global (..), Identifier (..), Local (..), Namespace (..), Qualifiedness (Q))
import Sweet.Name.Intrinsic (Intrinsic)

-- | How many parameters does this global have?
type Arity = Word

-- | Mangle a global or extract the intrinsic if it is an intrinsic. Intrinsics
-- are never mangled, because they are never referred to by name in the
-- generated code.
mangleGlobal :: Global 'Q -> Arity -> Either Intrinsic Text
mangleGlobal = (fmap postmangle .) . premangleGlobal

-- | Mangle a global, but with the fields already extracted.
mangleGlobal' :: Namespace -> Identifier -> Arity -> Text
mangleGlobal' = ((postmangle .) .) . premangleGlobal'

-- | Mangle a local.
mangleLocal :: Local -> Text
mangleLocal = postmangle . premangleLocal

-- | Premangle an identifier, for inclusion into a larger mangling.
premangleIdentifier :: Identifier -> Text
premangleIdentifier (Identifier i) = Text.pack (show (Text.length i)) <> "i" <> i
premangleIdentifier OperatorAsterisk = "8oasterisk"
premangleIdentifier OperatorLessGreater = "11olessgreater"
premangleIdentifier OperatorMinus = "5ominus"
premangleIdentifier OperatorPlus = "4oplus"
premangleIdentifier OperatorSlash = "5oslash"

-- | Premangle a global.
premangleGlobal :: Global 'Q -> Word -> Either Intrinsic Text
premangleGlobal (Qualified ns n) a = Right $ premangleGlobal' ns n a
premangleGlobal (Intrinsic i) _ = Left i

-- | Premangle a global, but with the fields already extracted.
premangleGlobal' :: Namespace -> Identifier -> Word -> Text
premangleGlobal' (Namespace ns) n a =
	Text.pack (show a) <> "g" <>
	foldMap premangleIdentifier (ns <> [n])

-- | Premangle a local.
premangleLocal :: Local -> Text
premangleLocal (Local l) = premangleIdentifier l
premangleLocal (SynthesizedLocal i) = "s" <> Text.pack (show i)

-- | Postmangle the output of premangling.
postmangle :: Text -> Text
postmangle t = Text.toLower . Text.take 63 $ "swt" <> hash t <> t
	where hash = base36 . fnv1a . fmap ord . Text.unpack

base36 :: Word32 -> Text
base36 0 = ""
base36 n =
	let (n', n'') = (n `div` 36, n `mod` 36) in
	base36 n' <> Text.singleton (table `genericIndex` n'')
	where table = "abcdefghijklmnopqrstuvwxyz0123456789"
