-- | Intrinsics are types and functions built into the compiler. The code
-- generators recognize intrinsics and generate appopriate code in them.
--
-- Some intrinsics, such as the arithmetic ones, are quite customizable,
-- offering a variety of strategies for dealing with different data
-- representations and specifying edge cases.
module Sweet.Name.Intrinsic
	( -- * Intrinsics
	  Intrinsic (..)
	, intrinsics
	, intrinsicName

	  -- * Integer options
	, IntWidth (..)
	, Signedness (..)
	, Overflow (..)
	, ZeroDivision (..)

	  -- * Floating-point options
	, FloatWidth (..)
	, FloatMode (..)
	) where

import Enumerate (Enumerable, enumerated)
import Data.Map.Strict (Map)
import Data.Function ((&))
import Data.Proxy (Proxy (..))
import Data.Semigroup ((<>))
import Data.Text (Text)
import GHC.Generics (Generic)
import GHC.TypeLits (KnownSymbol, symbolVal)

import qualified Data.Map.Strict as Map
import qualified Data.Text as Text
import qualified GHC.Generics as G

-- | Name of an intrinsic.
data Intrinsic :: * where
	-- | Record type constructor.
	RecordIntrinsic :: Intrinsic

	-- | Variant type constructor.
	VariantIntrinsic :: Intrinsic

	-- | Morphism from initial object.
	AbsurdIntrinsic :: Intrinsic

	-- | Text type.
	TextIntrinsic :: Intrinsic

	-- | Unicode scalar value type.
	USVIntrinsic :: Intrinsic

	-- | 32-bit integer type.
	IntIntrinsic :: Intrinsic

	-- | Addition of integers.
	AddIntrinsic :: IntWidth -> Signedness -> Overflow -> Intrinsic

	-- | Subtraction of integers.
	SubIntrinsic :: IntWidth -> Signedness -> Overflow -> Intrinsic

	-- | Multiplication of integers.
	MulIntrinsic :: IntWidth -> Signedness -> Overflow -> Intrinsic

	-- | Division of integers.
	DivIntrinsic :: IntWidth -> Signedness -> Overflow -> ZeroDivision -> Intrinsic

	-- | Natural logarithm of floating-point numbers.
	LnIntrinsic :: FloatWidth -> FloatMode -> Intrinsic

	-- | Bytes type.
	BytesIntrinsic :: Intrinsic

	-- | Concatenation of bytes.
	CatBytesIntrinsic :: Intrinsic

	-- | I/O action type.
	IOIntrinsic :: Intrinsic

	-- | Morphism mapping for I/O functor.
	MapIOIntrinsic :: Intrinsic

	-- | Injection for I/O applicative.
	PointIOIntrinsic :: Intrinsic

	-- | Join for I/O monad.
	JoinIOIntrinsic :: Intrinsic

	-- | Throw an exception in I/O.
	ThrowIOIntrinsic :: Intrinsic

	-- | Catch an exception in I/O.
	CatchIOIntrinsic :: Intrinsic

	-- | Unsafely perform an I/O action.
	PerformIOIntrinsic :: Intrinsic

	-- | Spawn a new thread for I/O.
	ForkIOIntrinsic :: Intrinsic

deriving stock instance Eq Intrinsic
deriving stock instance Ord Intrinsic
deriving stock instance Show Intrinsic
deriving stock instance Generic Intrinsic
deriving anyclass instance Enumerable Intrinsic

-- | Width of an integer.
data IntWidth
	= Byte -- ^ The intrinsic operates on 8-bit integers.
	| Short -- ^ The intrinsic operates on 16-bit integers.
	| Int -- ^ The intrinsic operates on 32-bit integers.
	| Long -- ^ The intrinsic operates on 64-bit integers.
	deriving stock (Eq, Ord, Bounded, Enum, Generic, Show)
	deriving anyclass (Enumerable)

-- | Signedness of an integer.
data Signedness
	= Unsigned -- ^ The intrinsic operates on unsigned integers.
	| Signed -- ^ The intrinsic operates on signed integers.
	deriving stock (Eq, Ord, Bounded, Enum, Generic, Show)
	deriving anyclass (Enumerable)

-- | What to do when overflow happens.
data Overflow
	= OverflowSignal -- ^ Signal overflow.
	| OverflowWrap -- ^ Wrap on overflow.
	| OverflowClamp -- ^ Clamp on overflow.
	| OverflowIndeterminate -- ^ Overflow will result in unspecified value.
	| OverflowUndefined -- ^ Undefined behavior upon arithmetic overflow.
	deriving stock (Eq, Ord, Bounded, Enum, Generic, Show)
	deriving anyclass (Enumerable)

-- | What to do when dividing by zero.
data ZeroDivision
	= ZeroDivisionSignal -- ^ Signal zero division.
	| ZeroDivisionZero -- ^ Zero division will result in zero.
	| ZeroDivisionIndeterminate -- ^ Zero division will result in unspecified value.
	| ZeroDivisionUndefined -- ^ Undefined behavior upon zero division.
	deriving stock (Eq, Ord, Bounded, Enum, Generic, Show)
	deriving anyclass (Enumerable)

-- | Width of a floating-point value.
data FloatWidth
	= Float -- ^ The intrinsic operates on 32-bit floating-point numbers.
	| Double -- ^ The intrinsic operates on 64-bit floating-point numbers.
	deriving stock (Eq, Ord, Bounded, Enum, Generic, Show)
	deriving anyclass (Enumerable)

-- | Which floating-point specification to conform to, if any.
data FloatMode
	-- | Perform math operations conforming to the IEEE 754 specification.
	= IEEE754Math

	-- | Perform math operations assuming properties such as associativity,
	-- no NaN values, and no infinities.
	| FastMath

	deriving stock (Eq, Ord, Bounded, Enum, Generic, Show)
	deriving anyclass (Enumerable)

-- | All intrinsics, by their names.
intrinsics :: Map Text Intrinsic
intrinsics = Map.fromList [ (intrinsicName i, i) | i <- enumerated ]

-- | Given an intrinsic, its name.
intrinsicName :: Intrinsic -> Text
intrinsicName i =
	let rep = G.from i in
	let class_ = gClass rep in
	let flags = gFlags rep in
	class_ <> flags

class GClass a where
	gClass :: a p -> Text

instance (GClass a) => GClass (G.M1 G.D c a) where
	gClass (G.M1 a) = gClass a

instance (GClass a, GClass b) => GClass (a G.:+: b) where
	gClass (G.L1 a) = gClass a
	gClass (G.R1 a) = gClass a

instance (KnownSymbol c) => GClass (G.M1 G.C ('G.MetaCons c f r) a) where
	gClass _ =
		symbolVal @c Proxy
		& Text.pack
		& Text.dropEnd (Text.length "Intrinsic")
		& Text.toLower

class GFlags a where
	gFlags :: a p -> Text

instance (GFlags a) => GFlags (G.M1 i c a) where
	gFlags (G.M1 a) = gFlags a

instance GFlags G.U1 where
	gFlags G.U1 = ""

instance (GFlags a, GFlags b) => GFlags (a G.:+: b) where
	gFlags (G.L1 a) = gFlags a
	gFlags (G.R1 a) = gFlags a

instance (GFlags a, GFlags b) => GFlags (a G.:*: b) where
	gFlags (a G.:*: b) = gFlags a <> gFlags b

instance (GFlag a) => GFlags (G.K1 i a) where
	gFlags (G.K1 a) = gFlag a

class GFlag a where
	gFlag :: a -> Text

instance GFlag IntWidth where
	gFlag Byte = "b"
	gFlag Short = "s"
	gFlag Int = "i"
	gFlag Long = "l"

instance GFlag Signedness where
	gFlag Unsigned = "u"
	gFlag Signed = "s"

instance GFlag Overflow where
	gFlag OverflowSignal = "s"
	gFlag OverflowWrap = "w"
	gFlag OverflowClamp = "c"
	gFlag OverflowIndeterminate = "i"
	gFlag OverflowUndefined = "u"

instance GFlag ZeroDivision where
	gFlag ZeroDivisionSignal = "s"
	gFlag ZeroDivisionZero = "z"
	gFlag ZeroDivisionIndeterminate = "i"
	gFlag ZeroDivisionUndefined = "u"

instance GFlag FloatWidth where
	gFlag Float = "f"
	gFlag Double = "d"

instance GFlag FloatMode where
	gFlag IEEE754Math = "i"
	gFlag FastMath = "f"
