{-# LANGUAGE MagicHash #-}
{-# LANGUAGE TemplateHaskell #-}

-- | Find all the globals defined in a translation unit. This is used to
-- generate the interface file for a translation unit, which is later fed into
-- the compiler when compiling dependent translation units. It is also used to
-- prepare for type inference of expressions, which need to know about all the
-- global definitions in the translation unit, because they may refer to
-- globals that are defined lexically after the expression in question.
module Sweet.Analyze.CollectGlobals
	( Globals (..), globalVars, globalTypes, globalDomains, Arity
	, collectTranslationUnit
	, collectDefinition
	, collectIntrinsics
	) where

import Control.Lens (makeLenses)
import Data.List (genericLength)
import Data.Map.Strict (Map)
import Data.Semigroup ((<>))

import qualified Data.Map.Strict as Map

import Sweet.Name (Global (..), Local, Namespace, Qualifiedness (..), rootNamespace, snocNamespace)
import Sweet.Name.Intrinsic (Intrinsic)
import Sweet.Syntax.Source (Definition (..), Kind (..), TranslationUnit (..), Type (..))

import qualified Sweet.Name.Intrinsic as I

-- | A mapping from globals to their types. The globals are paired with their
-- arities, because overloading on arity is allowed.
data Globals = Globals
	{ _globalVars :: Map (Global 'Q, Arity) (Type 'Q)
	, _globalTypes :: Map (Global 'Q, Arity) (Kind 'Q)
	, _globalDomains :: Map (Global 'Q) (Type 'Q) }
	deriving stock (Eq, Show)

instance Semigroup Globals where
	(<>) = mappend

instance Monoid Globals where
	mempty = Globals Map.empty Map.empty Map.empty
	mappend (Globals a1 b1 c1) (Globals a2 b2 c2) =
		Globals (a1 <> a2) (b1 <> b2) (c1 <> c2)

-- | How many parameters does this global have?
type Arity = Word

$(makeLenses ''Globals)

-- | Collect all globals in a translation unit.
collectTranslationUnit :: TranslationUnit f 'Q -> Globals
collectTranslationUnit (TranslationUnit ds) =
	foldMap (collectDefinition rootNamespace) ds <> collectIntrinsics

-- | Collect all globals in a definition.
collectDefinition :: Namespace -> Definition f 'Q -> Globals

collectDefinition ns (NamespaceDefinition n ds) =
	let ns' = snocNamespace ns n in
	foldMap (collectDefinition ns') ds

collectDefinition ns (DomainDefinition name type_) =
	let global = Qualified ns name in
	let kind = TypeKind in
	Globals Map.empty
		(Map.singleton (global, 0) kind)
		(Map.singleton global type_)

collectDefinition ns (FunctionDefinition name typeParams params retType _) =
	let global = (Qualified ns name, genericLength params) in
	let funcType = FunctionType (snd <$> params) retType in
	let type_ = foldr (uncurry ForAllType) funcType typeParams in
	Globals (Map.singleton global type_) Map.empty Map.empty

-- | All intrinsics.
collectIntrinsics :: Globals
collectIntrinsics = Globals (Map.fromList vars) (Map.fromList types) Map.empty
	where
	vars :: [((Global 'Q, Arity), Type 'Q)]
	vars =
		[ (i addisw#, 2) .= func
			[]
			[ i int# $$ []
			, i int# $$ [] ]
			( i int# $$ [] )
		, (i subisw#, 2) .= func
			[]
			[ i int# $$ []
			, i int# $$ [] ]
			( i int# $$ [] )
		, (i mulisw#, 2) .= func
			[]
			[ i int# $$ []
			, i int# $$ [] ]
			( i int# $$ [] )
		, (i diviswz#, 2) .= func
			[]
			[ i int# $$ []
			, i int# $$ [] ]
			( i int# $$ [] )

		, (i catbytes#, 2) .= func
			[]
			[ i bytes# $$ []
			, i bytes# $$ [] ]
			( i bytes# $$ [] )

		, (i mapio#, 2) .= func
			[ "e" .= TypeKind, "a" .= TypeKind, "b" .= TypeKind ]
			[ i io# $$ [l "e", l "a"]
			, func [] [l "a"] (l "b") ]
			( i io# $$ [l "e", l "b"] )
		, (i pointio#, 1) .= func
			[ "e" .= TypeKind, "a" .= TypeKind ]
			[ l "a" ]
			( i io# $$ [l "e", l "a"] )
		, (i joinio#, 1) .= func
			[ "e" .= TypeKind, "a" .= TypeKind ]
			[ i io# $$ [l "e", i io# $$ [l "e", l "a"]] ]
			( i io# $$ [l "e", l "a"] )
		, (i throwio#, 1) .= func
			[ "e" .= TypeKind, "a" .= TypeKind ]
			[ l "e" ]
			( i io# $$ [l "e", l "a"] )
		, (i catchio#, 2) .= func
			[ "e" .= TypeKind, "f" .= TypeKind, "a" .= TypeKind ]
			[ i io# $$ [l "e", l "a"]
			, func [] [l "e"] (i io# $$ [l "f", l "a"]) ]
			( i io# $$ [l "f", l "a"] )
		, (i forkio#, 1) .= func
			[ "e" .= TypeKind ]
			[ i io# $$ [voidT, unitT] ]
			( i io# $$ [l "e", unitT] )
		]

	types :: [((Global 'Q, Arity), Kind 'Q)]
	types =
		[ (i record#, 1) .= FunctionKind [RowKind TypeKind] TypeKind
		, (i variant#, 1) .= FunctionKind [RowKind TypeKind] TypeKind
		, (i int#, 0) .= TypeKind
		, (i bytes#, 0) .= TypeKind
		, (i io#, 2) .= FunctionKind [TypeKind, TypeKind] TypeKind
		]

	unitT = CallType (i record#) [EmptyRowType]
	voidT = CallType (i variant#) [EmptyRowType]

	record# = I.RecordIntrinsic
	variant# = I.VariantIntrinsic

	int# = I.IntIntrinsic
	addisw# = I.AddIntrinsic I.Int I.Signed I.OverflowWrap
	subisw# = I.SubIntrinsic I.Int I.Signed I.OverflowWrap
	mulisw# = I.MulIntrinsic I.Int I.Signed I.OverflowWrap
	diviswz# = I.DivIntrinsic I.Int I.Signed I.OverflowWrap I.ZeroDivisionZero

	bytes# = I.BytesIntrinsic
	catbytes# = I.CatBytesIntrinsic

	io# = I.IOIntrinsic
	mapio# = I.MapIOIntrinsic
	pointio# = I.PointIOIntrinsic
	joinio# = I.JoinIOIntrinsic
	throwio# = I.ThrowIOIntrinsic
	catchio# = I.CatchIOIntrinsic
	forkio# = I.ForkIOIntrinsic

	i :: Intrinsic -> Global 'Q
	i = Intrinsic

	l :: Local -> Type 'Q
	l = LocalType

	func :: [(Local, Kind 'Q)] -> [Type 'Q] -> Type 'Q -> Type 'Q
	func locals params ret =
		foldr (uncurry ForAllType) (FunctionType params ret) locals

	(.=) :: a -> b -> (a, b)
	(.=) = (,)

	($$) :: Global 'Q -> [Type 'Q] -> Type 'Q
	($$) = CallType
