-- | Code generation for the ECMAScript target.
module Sweet.Translate.ECMAScript
	( translateTranslationUnit
	, translateDefinition
	, translateExpression
	, translateValue
	) where

import Control.Monad.Writer (Writer)
import Data.Foldable (for_, traverse_)
import Data.List (genericLength)
import Data.Text (Text)

import qualified Control.Monad.Writer as Writer
import qualified Data.Text.Lazy.Builder as TB

import Sweet.Name (Namespace, Qualifiedness (Q), rootNamespace, snocNamespace)
import Sweet.Name.Intrinsic (Intrinsic)
import Sweet.Name.Mangle (mangleGlobal, mangleGlobal', mangleLocal)
import Sweet.Syntax.ANF (Definition (..), Expression (..), TranslationUnit (..), Value (..))

import qualified Sweet.Name.Intrinsic as I

-- | Translate a translation unit to ECMAScript statements.
translateTranslationUnit :: TranslationUnit Expression 'Q -> Writer TB.Builder ()
translateTranslationUnit (TranslationUnit ds) = do
	tellTX $ "\"use strict\";\n"
	traverse_ (translateDefinition rootNamespace) ds

-- | Translate a definition to ECMAScript statements.
translateDefinition :: Namespace -> Definition Expression 'Q -> Writer TB.Builder ()

translateDefinition ns (NamespaceDefinition n ds) =
	let ns' = snocNamespace ns n in
	traverse_ (translateDefinition ns') ds

translateDefinition _ (DomainDefinition _ _) =
	pure ()

translateDefinition ns (FunctionDefinition n _ ps _ b) = do
	tellTX $ "function "
	tellTX $ mangleGlobal' ns n (genericLength ps)
	tupled $ tellTX . mangleLocal . fst <$> ps
	tellTX $ " {\n"
	translateExpression "SWTreturn" b
	tellTX $ "return SWTreturn;\n"
	tellTX $ "}\n"

-- | Translate an expression to ECMAScript statements, defining the result to
-- the given variable.
translateExpression :: TB.Builder -> Expression 'Q -> Writer TB.Builder ()

translateExpression out (ValueExpression a) = do
	tellTB $ "var " <> out <> " = "
	translateValue a
	tellTX $ ";\n"

translateExpression out (LetExpression n v b) = do
	translateExpression (TB.fromText (mangleLocal n)) v
	translateExpression out b

translateExpression out (CallExpression f as) =
	case mangleGlobal f (genericLength as) of
		Left intrinsic ->
			translateIntrinsic out intrinsic as

		Right callee -> do
			tellTB $ "var " <> out <> " = "
			tellTX $ callee
			tupled $ translateValue <$> as
			tellTX $ ";\n"

translateExpression out (ApplyExpression f as) = do
	tellTB $ "var " <> out <> " = "
	translateValue f
	tupled $ translateValue <$> as
	tellTX $ ";\n"

-- | Translate a value to an ECMAScript expression. The expression has the
-- highest precedence, so parenthesizing it is not necessary.
translateValue :: Value 'Q -> Writer TB.Builder ()

translateValue (LocalValue a) =
	tellTX $ mangleLocal a

translateValue (USVLiteralValue a) =
	tellST $ show a

translateValue (TextLiteralValue a) =
	tellST $ show a

translateValue (LambdaValue ps b) = do
	tellTX $ "function"
	tupled $ tellTX . mangleLocal <$> ps
	tellTX $ " {\n"
	translateExpression "SWTreturn" b
	tellTX $ "return SWTreturn;\n"
	tellTX $ "}"

translateValue (WrapValue _ e) =
	translateValue e

translateValue (UnwrapValue _ e) =
	translateValue e

-- | Translate a call to an intrinsic to ECMAScript statements, defining the
-- result to the given variable.
translateIntrinsic :: TB.Builder -> Intrinsic -> [Value 'Q] -> Writer TB.Builder ()

translateIntrinsic out (I.AddIntrinsic I.Int I.Signed I.OverflowWrap) [a, b] = do
	tellTB $ "var " <> out <> " = "
	translateValue a
	tellTX $ " + "
	translateValue b
	tellTX $ " | 0;\n"

translateIntrinsic out (I.MulIntrinsic I.Int I.Signed I.OverflowWrap) [a, b] = do
	tellTB $ "var " <> out <> " = Math.imul("
	translateValue a
	tellTX $ ", "
	translateValue b
	tellTX $ ");\n"

translateIntrinsic out (I.DivIntrinsic I.Int I.Signed I.OverflowWrap I.ZeroDivisionZero) [a, b] = do
	tellTB $ "var " <> out <> " = "
	translateValue a
	tellTB $ " / "
	translateValue b
	tellTB $ " | 0;\n"

translateIntrinsic out (I.LnIntrinsic I.Double I.IEEE754Math) [x] = do
	tellTB $ "var " <> out <> " = Math.log("
	translateValue x
	tellTX $ ");\n"

translateIntrinsic out I.CatBytesIntrinsic [x, y] = do
	tellTB $ "var " <> out <> " = new Uint8Array("
	translateValue x
	tellTX $ ".length + "
	translateValue y
	tellTX $ ".length"
	tellTX $ ");\n"

	tellTB $ out <> ".set("
	translateValue x
	tellTX $ ", 0);\n"

	tellTB $ out <> ".set("
	translateValue y
	tellTX $ ", "
	translateValue x
	tellTX $ ".length);\n"

translateIntrinsic out I.MapIOIntrinsic [a, f] = do
	tellTB $ "var " <> out <> " = function(SWTsuccess, SWTfailure) {\n"
	translateValue a
	tellTX $ "(function(SWTresult) {\n"
	tellTX $ "SWTsuccess("
	translateValue f
	tellTX $ "(SWTresult));\n"
	tellTX $ "}, SWTfailure);\n"
	tellTX $ "};\n"

translateIntrinsic out I.PointIOIntrinsic [a] = do
	tellTB $ "var " <> out <> " = function(SWTsuccess, SWTfailure) {\n"
	tellTX $ "SWTsuccess("
	translateValue a
	tellTX $ ");\n"
	tellTX $ "};\n"

translateIntrinsic out I.JoinIOIntrinsic [a] = do
	tellTB $ "var " <> out <> " = function(SWTsuccess, SWTfailure) {\n"
	translateValue a
	tellTX $ "(function(SWTresult) {"
	tellTX $ "SWTresult(SWTsuccess, SWTfailure);\n";
	tellTX $ "}, SWTfailure);\n"
	tellTX $ "};\n"

translateIntrinsic out I.ThrowIOIntrinsic [e] = do
	tellTB $ "var " <> out <> " = function(SWTsuccess, SWTfailure) {\n"
	tellTX $ "SWTfailure("
	translateValue e
	tellTX $ ");\n"
	tellTX $ "};\n"

translateIntrinsic out I.CatchIOIntrinsic [a, k] = do
	tellTB $ "var " <> out <> " = function(SWTsuccess, SWTfailure) {\n"
	translateValue a
	tellTX $ "(SWTsuccess, function(SWTexception) {\n"
	translateValue k
	tellTX $ "(SWTexception)(SWTsuccess, SWTfailure);\n";
	tellTX $ "});\n"
	tellTX $ "};\n"

translateIntrinsic out I.ForkIOIntrinsic [a] = do
	tellTB $ "var " <> out <> " = function(SWTsuccess, SWTfailure) {\n"
	translateValue a
	tellTX $ "(function(SWTresult) { }, function(SWTexception) { });\n"
	tellTX $ "SWTsuccess(undefined);\n"
	tellTX $ "};\n"

translateIntrinsic _ _ _ = tellST "TODO"

tupled :: [Writer TB.Builder ()] -> Writer TB.Builder ()
tupled xs = do
	tellTX $ "("
	for_ ([id @Word 0 ..] `zip` xs) $ \case
		(0, x) -> x
		(_, x) -> tellTB ", " *> x
	tellTX $ ")"

tellST :: String -> Writer TB.Builder ()
tellST = Writer.tell . TB.fromString

tellTB :: TB.Builder -> Writer TB.Builder ()
tellTB = Writer.tell

tellTX :: Text -> Writer TB.Builder ()
tellTX = Writer.tell . TB.fromText
